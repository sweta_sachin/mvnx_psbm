package TestsOnLeftHandTabsOnCRM;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import Common_Methods.CommonMethods;


public class AccessLeftHandMenuForCustomer extends CommonMethods{
	@BeforeClass()
	public void Setup() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	  driver.quit();
	}
	
	@Test
	public void a_AccessAttachment_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_Attachment();
		
	}
	
	@Test
	public void b_AccessCommunication_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_Communication();
		
	}
	
	@Test
	public void c_AccessCreateSubscriber_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_CreateSubscriber();
		
	}
	
	@Test
	public void d_AccessCustomerDebugData_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_CustomerDebugData();
		
	}
	
	@Test
	public void e_AccessDirectDebit_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_DirectDebit();
		
	}
	
	@Test
	public void f_AccessEditCustomer_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_EditCustomer();
	}
	
	@Test
	public void g_AccessNotes_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_Notes();
	}
	
	@Test
	public void h_AccessNotification_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_Notification();
	}
	
	@Test
	public void z_AccessRegisteredDocument_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_RegisteredDocument();
		
	}
	
	@Test
	public void j_AccessAccountDiscount_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_AccountDiscounts();
	}
	
	@Test
	public void k_AccessAccountSubscription_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_AccountSubscription();
	}
	
	@Test
	public void l_Access_AdHoc_Charges_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_Ad_HOC_charges();
	}
	
	@Test
	public void m_Access_AutoBarrHistory_Charges_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_AutobarHistory();
	}
	
	@Test
	public void n_Access_DeactivateAccount_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_DeactivateAccount();
	}
	
	@Test
	public void o_Access_FinancialAdjustment_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_FinancialAdjustment();
	}
	
	@Test
	public void p_Access_InstantBillCustomer_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_InstantBillCustomer();
	}
	
	@Test
	public void q_Access_Sponser_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_sponser();
	}
	
	@Test
	public void r_Access_ViewInvoices_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1500);
		Access_ViewInvoices();
	}
	
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************************* Method to access Attachment from Left hand Menu *****************************************
public void  Access_Attachment() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
	
	WebElement attachment= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[1]/a[1]"));
	
	Boolean name= attachment.isDisplayed();
	
	if(name==true) {
	    
		wait.equals(name);
	    
		Actions act= new Actions(driver);
		
		act.moveToElement(attachment).doubleClick().build().perform();
		
		Thread.sleep(500);
		
		driver.navigate().back();
	  }
	}

//********************************************* Method to access communication from Left hand Menu *****************************************
public void  Access_Communication() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
		
	WebElement comms= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[2]/a[1]"));
	
	Boolean com= comms.isDisplayed();
	
	if(com==true)  {
	    
		wait.equals(com);
	    
		Actions act= new Actions(driver);
		
		act.moveToElement(comms).click().build().perform();
		
		Thread.sleep(500);
        
		driver.navigate().back();
	    
	}
}

//********************************************* Method to access create subscriber from Left hand Menu *****************************************
public void  Access_CreateSubscriber() throws InterruptedException
{
	WebElement Create_sub=driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[3]/a[1]"));
    
    Actions act= new Actions(driver);
    
    act.moveToElement(Create_sub).click().build().perform();

    Thread.sleep(500);
    
	WebElement file= driver.findElement(By.id("P34_BSD_NAME"));
	
	Boolean File= file.isDisplayed();
	
	driver.navigate().back();
		
}

//********************************************* Method to access customer debug data from Left hand Menu *****************************************
public void  Access_CustomerDebugData() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
			
	WebElement notes= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[4]/a[1]"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true) {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
	}
    
	Thread.sleep(500);
    
    driver.navigate().back();
  }

//********************************************* Method to access direct debit from Left hand Menu *****************************************
public void  Access_DirectDebit() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
	
	WebElement notes= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[5]/a[1]"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true)  {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).click().build().perform();
	}
		  
	 Thread.sleep(500);
	    		
	 driver.navigate().back();
	}


//********************************************* Method to access edit customer from Left hand Menu *****************************************
public void  Access_EditCustomer() throws InterruptedException
{
	WebElement editCust= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[6]/a[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(editCust).click().build().perform();
	      
	Thread.sleep(500);
	    
	 driver.navigate().back();
}

//********************************************* Method to access notes from Left hand Menu *****************************************
public void  Access_Notes() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);

	WebElement QuickNotes= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[7]/a[1]"));
	
	Boolean name= QuickNotes.isDisplayed();
	
	if(name==true)   {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		Thread.sleep(1500);
		
		act.moveToElement(QuickNotes).click().build().perform();
			
		Thread.sleep(1500);
	}
		   
	driver.navigate().back();
}


//********************************************* Method to access notification from Left hand Menu *****************************************
public void  Access_Notification() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);

	WebElement notes= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[8]/a[1]"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true)   {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).click().build().perform();
		
		Thread.sleep(1000);
	}
	   Thread.sleep(500);
	    
	    driver.navigate().back();
   }

//********************************************* Method to access registered documents from Left hand Menu *****************************************
public void Access_RegisteredDocument() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);

	WebElement doc= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[9]/a[1]"));
	
	Boolean name= doc.isDisplayed();
	
	if(name==true)  {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(doc).click().build().perform();
		
		Thread.sleep(1000);
	}
	
	Thread.sleep(500);
	    
	driver.navigate().back();
	
}

//********************************************* Method to access Account discount from Left hand Menu *****************************************
public void Access_AccountDiscounts() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement dis= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/ul[1]/li[1]/a[1]"));
	
	Boolean name= dis.isDisplayed();
	
	if(name==true)  {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(dis).click().build().perform();
		
		Thread.sleep(1000);
	}
	 		
	Thread.sleep(500);
	    
	driver.navigate().back();
}

//********************************************* Method to access Account subscription from Left hand Menu *****************************************
public void Access_AccountSubscription() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
	
	WebElement sub= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/ul[1]/li[2]/a[1]"));
	
	Boolean name= sub.isDisplayed();
	
	if(name==true)  {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(sub).click().build().perform();
		
		Thread.sleep(1000);
	}
	Thread.sleep(500);
	
	driver.navigate().back();
}

//********************************************* Method to access Ad-hoc-charges from Left hand Menu *****************************************
public void Access_Ad_HOC_charges() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement sub= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/ul[1]/li[3]/a[1]"));
	
	Boolean name= sub.isDisplayed();
	
	if(name==true)  {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(sub).click().build().perform();
		
		Thread.sleep(1000);
	}
	 		
	Thread.sleep(500);
	    
	driver.navigate().back();
}

//********************************************* Method to access Autobarr history from Left hand Menu *****************************************
public void Access_AutobarHistory() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
				
	WebElement sub= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/ul[1]/li[4]/a[1]"));
	
	Boolean name= sub.isDisplayed();
	
	if(name==true)   {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(sub).click().build().perform();
		
		Thread.sleep(1000);
		
	}
	
	Thread.sleep(500);
	    
	driver.navigate().back();
}

//********************************************* Method to access deactivate account from Left hand Menu *****************************************
public void Access_DeactivateAccount() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement sub= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/ul[1]/li[5]/a[1]"));
	
	Boolean name= sub.isDisplayed();
	
	if(name==true) {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(sub).click().build().perform();
		
		Thread.sleep(1000);
	}
	 		
	Thread.sleep(500);
	    
	driver.navigate().back();
}

//********************************************* Method to access financial adjustments from Left hand Menu *****************************************
public void Access_FinancialAdjustment() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
				
	WebElement sub= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/ul[1]/li[6]/a[1]"));
	
	Boolean name= sub.isDisplayed();
	
	if(name==true)  {
		    	
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(sub).click().build().perform();
		
		Thread.sleep(1000);
	}
	
	Thread.sleep(500);
	
	driver.navigate().back();
}

//********************************************* Method to access instant bill customer from Left hand Menu *****************************************
public void Access_InstantBillCustomer() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement sub= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/ul[1]/li[7]/a[1]"));
	
	Boolean name= sub.isDisplayed();
	
	if(name==true) {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(sub).click().build().perform();
		
		Thread.sleep(1000);
		}
	
	Thread.sleep(500);
	
	driver.navigate().back();
   }


//********************************************* Method to access sponser from Left hand Menu *****************************************
public void Access_sponser() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement sub= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/ul[1]/li[8]/a[1]"));
	
	Boolean name= sub.isDisplayed();
	
	if(name==true)   {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(sub).click().build().perform();
		
		Thread.sleep(1000);
	}
	Thread.sleep(500);
	    
	driver.navigate().back();
}

//********************************************* Method to access View invoice tab from Left hand Menu *****************************************
public void Access_ViewInvoices() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement sub= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/ul[1]/li[9]/a[1]"));
	
	Boolean name= sub.isDisplayed();
	
	if(name==true)  {
		    	
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(sub).click().build().perform();
		
		Thread.sleep(1000);
	}
	Thread.sleep(500);
	    
	driver.navigate().back();
}

}
