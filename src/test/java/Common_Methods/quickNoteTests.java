package Common_Methods;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;

public class quickNoteTests extends CommonMethods{
	public static String subject= "changed subject";
	
////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to click quick notes **********************************
public void ClickQuickNotes() throws InterruptedException
{  
	try {
	
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
		
		WebElement QuickNotes= driver.findElement(By.id("B13528763358177637"));
		
		Boolean name= QuickNotes.isDisplayed();
		
		if(name==true)  {
			
			wait.equals(name);
			
			Actions act= new Actions(driver);
			
			Thread.sleep(1500);
			
			act.moveToElement(QuickNotes).click().build().perform();
			
			Thread.sleep(1500);
		}
	}catch(StaleElementReferenceException e)    {
		
		WebElement QuickNotes= driver.findElement(By.id("B13528763358177637"));
		
		Actions act= new Actions(driver);
		
		Thread.sleep(1500);
		
		act.moveToElement(QuickNotes).click().build().perform();
		
		Thread.sleep(1500);
	}
}

//********************************** Method to Click create Quick notes********************************
public void ClickCreateQuickNote()
{
	try {
	
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
		
		WebElement create= driver.findElement(By.linkText("Create"));
	    
		Boolean name= create.isDisplayed();
	    
		if(name==true)   {
	    
			wait.equals(name);
	    
			Actions act= new Actions(driver);
		
			act.moveToElement(create).click().build().perform();
		
		}
	
	}catch(StaleElementReferenceException e) {
	
		WebElement create= driver.findElement(By.linkText("Create"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(create).click().build().perform();
	  }
  }
	
//*************************************  Method to select type *************************************
public void Select_Type()
{
	Select dropdown= new Select(driver.findElement(By.id("P25_QN_QNT_UID")));

	dropdown.selectByIndex(3);
		
}
	
//*************************************  Method to enter subject *************************************	
public void Enter_subject()
{
	driver.findElement(By.id("P25_QN_SUBJECT")).sendKeys("new subject");

}
	
//*************************************  Method to enter note *************************************	
public void Enter_note()
{
	driver.findElement(By.id("P25_DETAIL")).sendKeys("test note data");
		
}
	
//*************************************  Method to select status *************************************
public void Select_status()
{
	driver.findElement(By.id("P25_QN_SS_UID_5")).click();

}

//*************************************  Method to fault type *************************************
public void Select_faultType()
{
	Select dropdown= new Select(driver.findElement(By.id("P25_QN_FAULT_QNT_UID")));

	dropdown.selectByIndex(2);
	
}
	
//*************************************  Method to clickCreate note *************************************
public void Click_create()
{
	driver.findElement(By.id("B13529453749207036")).click();

}

//********************************* Method to check success message ***************************
public void suceess_msg()
{
	WebElement msg= driver.findElement(By.id("echo-message"));

	String MSG= msg.getText();
	
	String Exp_Msg= "Action Processed.";

	Assert.assertEquals(MSG, Exp_Msg);
	
}

//************************************* Method to verify created quick note ************************
 public void Verify_note()
 {
	 WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[10]"));

	 String DATE= datestamp.getText();
	
	 System.out.println(DATE);
	
	 DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	
	 Date date = new Date();
	
	 String date1= dateFormat.format(date);
	
	 System.out.println(date1);
	
	 String Date2= DATE.substring(0, 17);
	
	 Assert.assertEquals(Date2, date1);
	 
  }
}
