package CreditControlTab;

import org.testng.annotations.Test;

import Common_Methods.CommonMethods;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class Create_CustomerTests extends CommonMethods {
	
	
	@BeforeClass
	public void testCreateCustomer() throws InterruptedException
	{
		Login();
		Thread.sleep(1000);
		clickCreditControlTab();
		Thread.sleep(500);
		ClickCreateCustomer();
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
		driver.manage().timeouts().implicitlyWait(2,TimeUnit.SECONDS);
		driver.quit();
	}
	
	@Test
	public void A_SelectTitile()
	{
	    Select dropdown= new Select(driver.findElement(By.id("P40_BCD_T_UID")));
	    dropdown.selectByIndex(2);;
	    
	}
	
	@Test
	public void aEnterFirstName() throws InterruptedException
	{
		Thread.sleep(500);
	  EnterFirstName();
				
	}
	
	@Test
	public void aaEnterAccountName() throws InterruptedException
	{
		Thread.sleep(500);
		EnterAccountName();
		
	}
	
	@Test
	public void abEnterCompanyName() throws InterruptedException
	{
		Thread.sleep(500);
		
		WebElement Account_name= driver.findElement(By.id("P40_BCD_COMPANY_NAME"));
		Account_name.sendKeys("Company A");
	}
	
	@Test
	public void bEnterContactNumber() throws InterruptedException
	{
		Thread.sleep(500);
		EnterContactNumber();
	}
	
	
	@Test
	public void bbSelectInvoiceDeliveryMethod() throws InterruptedException
	{
		Thread.sleep(500);
		SelectInvoiceDeliveryMethod();
	}
	
	@Test
	public void cSelectCreditController() throws InterruptedException
	{
		Thread.sleep(800);
		SelectCreditController();
	}
	
	@Test
	public void dSelectTaxRate() throws InterruptedException
	{
		Thread.sleep(500);
		SelectTaxRate();
	}
	
	@Test
	public void eSelectStatementType() throws InterruptedException
	{
		Thread.sleep(500);
		SelectStatementType();
	}
	
	@Test
	public void fSelectSalesPerson() throws InterruptedException
	{
		Thread.sleep(500);
		SelectSalesPerson();
	}
	
	@Test
	public void gSelectdeliveryAddress() throws InterruptedException
	{
		Thread.sleep(500);
		SelectdeliveryAddress();
	}
	@Test
	public void ggEnterAddress() throws InterruptedException
	{
		Thread.sleep(500);
		EnterAddress();
	}
	
	@Test
	public void hSelectBillCycle() throws InterruptedException
	{
		Thread.sleep(500);
		SelectBillCycle();
	}
	
	@Test
	public void iSelectAutoBarRules() throws InterruptedException
	{
		Thread.sleep(500);
		SelectAutoBarRules();
	}
	
	@Test
	public void jSelectBillExtractCycle() throws InterruptedException
	{
		Thread.sleep(500);
		SelectBillExtractCycle();
	}
	
	@Test
	public void kSelectAccountType() throws InterruptedException
	{
		Thread.sleep(500);
		SelectAccountType();
	}
	
	@Test
	public void ka_selectCustomerType()
	{
		ClickCustomerType();
	}
	
	@Test
	public void lEnterLastName() throws InterruptedException
	{
		Thread.sleep(500);
		EnterLastName();
	}
	
	@Test
	public void mEnterContactName() throws InterruptedException
	{
		Thread.sleep(500);
		EnterContactName();
	}
	
	@Test
	public void nSelectBank() throws InterruptedException
	{
		Thread.sleep(500);
		SelectBank();
	}
	
	

	@Test
	public void pClickApplyChanges() throws InterruptedException
	{ 
		Thread.sleep(500);
		ClickApplyChanges();
	}

	@Test
	public void zsuccess() throws InterruptedException
	{
		Thread.sleep(1000);
		success();
	} 
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
//***************************** Method to enter first name **************************
public void EnterFirstName() throws InterruptedException
 {
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);

	WebElement F_name= driver.findElement(By.id("P40_BCD_FIRST_NAME"));
	
	Boolean name= F_name.isDisplayed();
	
	wait.equals(name);
	
	F_name.sendKeys("abcd");
   }

//************************** Method to enter account name ****************************
public void EnterAccountName()
{
	try {
	
		WebElement Account_name= driver.findElement(By.id("P40_BCD_NAME"));
	
		Account_name.sendKeys("Name ABC");
	
	}catch (NoSuchElementException| StaleElementReferenceException e) {
		
		WebElement Account_name= driver.findElement(By.id("P40_BCD_NAME"));
		
		Account_name.sendKeys("NewNAME");
	}
}

//************************** Method to enter contact number ***************************
public void EnterContactNumber()
{
	try {
		
		WebElement Contact_number= driver.findElement(By.id("P40_BCD_TELE_NO1"));
		
		Contact_number.sendKeys("0740404040");
		
	}catch (NoSuchElementException| StaleElementReferenceException e){
			
		WebElement Contact_number= driver.findElement(By.id("P40_BCD_TELE_NO1"));
		
		Contact_number.sendKeys("0740404040");
		}
	}

//*************************** Method to select invoice delivery method *******************
public void SelectInvoiceDeliveryMethod()
{
	try {
		
		Select InvoiceDelivery= new Select (driver.findElement(By.id("P40_BCD_DM_UID")));
		
		InvoiceDelivery.selectByVisibleText("SMS");
		
	}catch (NoSuchElementException| StaleElementReferenceException e) {
			
		Select InvoiceDelivery= new Select (driver.findElement(By.id("P40_BCD_DM_UID")));
		
		InvoiceDelivery.selectByVisibleText("SMS");
		}
	}

//*************************** Method to select credit controller *******************
public void SelectCreditController()
{ 
	try {
	
		Select creditController= new Select (driver.findElement(By.id("P40_BCD_CRED_UID")));
	
		creditController.selectByVisibleText("NON");
	
	}catch (NoSuchElementException| StaleElementReferenceException e)	{
		
		Select creditController= new Select (driver.findElement(By.id("P40_BCD_CRED_UID")));
		
		creditController.selectByVisibleText("NON");
	}
}

//*************************** Method to select tax rate *******************************
public void SelectTaxRate()
{
	try {
	
		Select TaxRate= new Select (driver.findElement(By.id("P40_BCD_TR_UID")));
	
		TaxRate.selectByVisibleText("Standard VAT 15%");
	
	}catch (NoSuchElementException| StaleElementReferenceException e) {
		
		Select TaxRate= new Select (driver.findElement(By.id("P40_BCD_TR_UID")));
		
		TaxRate.selectByVisibleText("Standard VAT 15%");
	}
}
//*************************** Method to select statement type ***************************	
public void SelectStatementType()
{   
	try {
	
		Select Statement= new Select (driver.findElement(By.id("P40_BCD_STATEMENT_TYPE")));
	
		Statement.selectByVisibleText("Single");
	
	}catch(NoSuchElementException| StaleElementReferenceException e) {
		
		Select Statement= new Select (driver.findElement(By.id("P40_BCD_STATEMENT_TYPE")));
		
		Statement.selectByVisibleText("Single");
	}
}

//*************************** Method to select Sales person ******************************
public void SelectSalesPerson()
{
	try {
	
		Select creditController= new Select (driver.findElement(By.id("P40_BCD_SAP_UID")));
	
		creditController.selectByIndex(1);
	
	}catch (NoSuchElementException| StaleElementReferenceException e){
		
		Select creditController= new Select (driver.findElement(By.id("P40_BCD_SAP_UID")));
		
		creditController.selectByIndex(1);
	}
}

//*************************** Method to select delivery address ***************************
public void SelectdeliveryAddress()
{
	try {
	
		driver.findElement(By.id("P40_DELIVERY_ADDRESS_0")).click();
    
	}catch(NoSuchElementException| StaleElementReferenceException e) {
    	
		driver.findElement(By.id("P40_DELIVERY_ADDRESS_0")).click();	
		}
	}
	
//********************************* Method to enter address *********************************
public void EnterAddress()
{ 
	try {
	
		driver.findElement(By.id("P40_ADDRESS_LEFT_1")).sendKeys("xyz");
	
		driver.findElement(By.id("P40_ADDRESS_LEFT_2")).sendKeys("ABCD");
	
		driver.findElement(By.id("P40_ADDRESS_LEFT_3")).sendKeys("North Riding");
	
		driver.findElement(By.id("P40_ADDRESS_LEFT_4")).sendKeys("Johannesburg");
	
		driver.findElement(By.id("P40_ADDRESS_LEFT_5")).sendKeys("Gauteng");
	
		driver.findElement(By.id("P40_ADDRESS_LEFT_6")).sendKeys("2124");
	
	}catch(NoSuchElementException| StaleElementReferenceException e) {
		
		driver.findElement(By.id("P40_ADDRESS_LEFT_1")).sendKeys("xyz");
		
		driver.findElement(By.id("P40_ADDRESS_LEFT_2")).sendKeys("ABCD");
		
		driver.findElement(By.id("P40_ADDRESS_LEFT_3")).sendKeys("North Riding");
		
		driver.findElement(By.id("P40_ADDRESS_LEFT_4")).sendKeys("Johannesburg");
		
		driver.findElement(By.id("P40_ADDRESS_LEFT_5")).sendKeys("Gauteng");
		
		driver.findElement(By.id("P40_ADDRESS_LEFT_6")).sendKeys("2124");
	}
}

//*************************** Method to select Bill cycle ***********************************
public void SelectBillCycle()
{
	try {
	
		Select BillCycle= new Select (driver.findElement(By.id("P40_BCD_BBC_UID")));
	
		BillCycle.selectByVisibleText("Main Bill Cycle");
	
	}catch(NoSuchElementException| StaleElementReferenceException e)	{
		
		Select BillCycle= new Select (driver.findElement(By.id("P40_BCD_BBC_UID")));
		
		BillCycle.selectByVisibleText("Main Bill Cycle");
	}
}

//*************************** Method to select Autobar rule ********************************
public void SelectAutoBarRules()
{
	try {
		
	  Select AutoBar= new Select (driver.findElement(By.id("P40_BCD_ABR_UID")));
	 
	  AutoBar.selectByVisibleText("No Barring");
	
	}catch (NoSuchElementException| StaleElementReferenceException e) {
		
		Select AutoBar= new Select (driver.findElement(By.id("P40_BCD_ABR_UID")));
		
		AutoBar.selectByVisibleText("No Barring");
	}
}

//*************************** Method to select Bill extract cycle *****************************
public void SelectBillExtractCycle()
{
	try {
	
		Select BillExtractCycle= new Select (driver.findElement(By.id("P40_BCD_EXC_UID")));
	
		BillExtractCycle.selectByVisibleText("PSBM Extract Cycle");
	
	}catch (NoSuchElementException| StaleElementReferenceException e)	{
		
		Select BillExtractCycle= new Select (driver.findElement(By.id("P40_BCD_EXC_UID")));
		
		BillExtractCycle.selectByVisibleText("PSBM Extract Cycle");
	}
}

//*************************** Method to select Account type ************************************
public void SelectAccountType()
{  
	try {
	
		Select AccountType= new Select (driver.findElement(By.id("P40_BCD_ACCT_UID")));
	
		AccountType.selectByVisibleText("NASASA Member");
	
	} catch (NoSuchElementException| StaleElementReferenceException e)	{
		
		Select AccountType= new Select (driver.findElement(By.id("P40_BCD_ACCT_UID")));
		
		AccountType.selectByVisibleText("NASASA Member");
	}
}

//*************************** Method to enter last name *********************************************
public void EnterLastName()
{ 
	try {

		driver.findElement(By.id("P40_BCD_SURNAME")).sendKeys("Surname");
	
	}  catch (NoSuchElementException| StaleElementReferenceException e)	{
		
		driver.findElement(By.id("P40_BCD_SURNAME")).sendKeys("Surname");
	}
}

//********************************** Method to enter contact name  ************************************
public void EnterContactName()
{
	try {
	
		driver.findElement(By.id("P40_BCD_CONTACT_NAME")).sendKeys("contact name");
	
	}catch (NoSuchElementException| StaleElementReferenceException e)	{
		
		driver.findElement(By.id("P40_BCD_CONTACT_NAME")).sendKeys("contact name");
	}
}

//********************************* Method to select Bank ****************************************
public void SelectBank()
{
	try {
	
		Select Bank= new Select (driver.findElement(By.id("P40_BCD_B_UID")));
	
		Bank.selectByVisibleText("CASH");
	
	}catch (NoSuchElementException| StaleElementReferenceException e)	{
		
		Select Bank= new Select (driver.findElement(By.id("P40_BCD_B_UID")));
		
		Bank.selectByVisibleText("CASH");
	}
}

//*************************** Method to click customer type **********************************
public void ClickCustomerType()
{ 
	try {
   
		WebElement ctype= driver.findElement(By.id("P40_BCD_TYPE_2"));
    
		Actions act= new Actions(driver);
    
		act.moveToElement(ctype).click().build().perform();
	
	} catch (NoSuchElementException| StaleElementReferenceException e)	{
		 
		WebElement ctype= driver.findElement(By.id("P40_BCD_TYPE_2"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(ctype).click().build().perform();
	}
  
} 
//*************************** Method to click apply changes **********************************
public void ClickApplyChanges()
{ 
	try {
		
		driver.findElement(By.id("B13363654567673223")).click();
	
	}catch (NoSuchElementException| StaleElementReferenceException e)	{
 	
		driver.findElement(By.id("B13363654567673223")).click();
	}
}

//*************************** Method to check success message **********************************
public void success()
{
	WebElement Cdetails= driver.findElement(By.linkText("Customer Address Details"));
	
	Boolean CDetails= Cdetails.isDisplayed();
	
	Assert.assertTrue(CDetails);
  }
}
