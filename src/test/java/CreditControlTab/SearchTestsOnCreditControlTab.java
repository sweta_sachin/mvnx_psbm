package CreditControlTab;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import Common_Methods.CommonMethods;


public class SearchTestsOnCreditControlTab extends CommonMethods{
	
	public static String AccName= "PSB-BulkRICA";
	
	public static String AccNumber= "PSB0000001";
	
	public static String CNumber= "0740844529";
	
	public static String AcType= "NASASA Member";
	
	public static String TaxRate= "Standard VAT 15%";
	
	@BeforeClass()
	public void CRM() throws InterruptedException	{
		Login();
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() 
	{
	  driver.quit();
		}
	
	@Test
	public void a_ViewCreditContol_tab() throws InterruptedException {
		Thread.sleep(1000);
		View_Credit_Control();
	}
	
	@Test
	public void b_Click_CreditControl_tab() {
		click_credit_control();		
	}
	
	@Test
	public void c_Search_BY_AccountNameOn_Credit_Control() throws InterruptedException 
	{
		SearchByAccountName();
	}
	
	@Test
	public void d_Search_BY_AccountNumberOn_Credit_Control() throws InterruptedException
	{
		SearchByAccountNumber();
	}
	
	@Test
	public void e_Search_BY_PhoneNumberOn_Credit_Control() throws InterruptedException
	{
		SearcByTelephNumber();
	}
	
	@Test
	public void f_Search_BY_Account_TypeOn_Credit_Control() throws InterruptedException
	{
		SearchByAccountType();
	}
	
	@Test
	public void g_Search_BY_TaxRate_on_Credit_Control() throws InterruptedException
	{
		SearchByTaxRate();
	}
	
	@Test
	public void h_Search_BY_StatementType_on_Credit_Control() throws InterruptedException
	{
		SearchByStatementType();
	}
	
		
	@Test
	public void z_Search_Customer_info_OnCredit_Control_Tab() throws InterruptedException
	{
		Thread.sleep(1500);
		click_search();
		Thread.sleep(1500);
		Boolean ACName =driver.findElement(By.linkText("Account Name")).isDisplayed();
		 Assert.assertTrue(ACName);
		
	}
	
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************* Method to view Credit Control tab ****************************
public void View_Credit_Control()
{
	Boolean CreditControl= driver.findElement(By.linkText("Credit Control")).isDisplayed();
	
	Assert.assertTrue(CreditControl);
	
}
	
//****************************************** Method to click credit control tab ****************************
public void click_credit_control() 
{
	clickCreditControlTab() ;
	
	List<WebElement> anchors = driver.findElements(By.tagName("a"));
	
	java.util.Iterator<WebElement> i = anchors.iterator();
	
	while(i.hasNext()) {
	
		WebElement anchor = i.next();                 
		
		if(anchor.getText().contains("Create Customer"))  {
		    	  
			WebElement element= driver.findElement(By.linkText("Create Customer"));
			
			Boolean Element= element.isDisplayed();
			
			Assert.assertTrue(Element); 
		    
			break;
		    
		}else {
		
			System.out.println(" Create customer tab Not displayed");
		}
	}
}

//************************** Method to click Search on Credit control tab ***************************
public void click_search() {
	try {
	
		WebElement search= driver.findElement(By.id("B26832618095098737"));
	
		Actions act= new Actions(driver);
	
		act.moveToElement(search).doubleClick().build().perform();
	
		act.moveToElement(search).doubleClick().build().perform();
	
	}catch(StaleElementReferenceException e){
		
		WebElement search= driver.findElement(By.id("B26832618095098737"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(search).doubleClick().build().perform();
	  }
}

//*********************** Method to search by account name on Credit control tab *****************
public void SearchByAccountName() throws InterruptedException
{
	driver.findElement(By.id("P7_ACC_NAME")).clear();
	
	driver.findElement(By.id("P7_ACC_NAME")).sendKeys(AccName);
	
	click_search();
	
	Thread.sleep(1500);
	
	String ACName =driver.findElement(By.xpath("//div[@id='R2806414137352331']//tr[2]//td[3]")).getText();
	
	driver.findElement(By.id("P7_ACC_NAME")).clear();
	
	Assert.assertEquals(ACName, AccName);
		
}

//********************** Method to search by account number on credit control tab ****************
public void SearchByAccountNumber() throws InterruptedException
{
	driver.findElement(By.id("P7_ACC_NO")).clear();

	driver.findElement(By.id("P7_ACC_NO")).sendKeys(AccNumber);
	
	click_search();
	
	Thread.sleep(4000);
	
	String ACNum =driver.findElement(By.xpath("//div[@id='R2806414137352331']//td[2]//a[1]")).getText();
	
	driver.findElement(By.id("P7_ACC_NO")).clear();
	
	Assert.assertEquals(ACNum, AccNumber);
	
}

//********************* Method to search by telephone number on credit control tab ***************
public void SearcByTelephNumber() throws InterruptedException
{
	driver.findElement(By.id("P7_TELE_NO")).clear();

	driver.findElement(By.id("P7_TELE_NO")).sendKeys(CNumber);
	
	click_search();
	
	Thread.sleep(2500);
	
	String TeleNum =driver.findElement(By.xpath("//div[@id='R2806414137352331']//tr[2]//td[7]")).getText();
	
	driver.findElement(By.id("P7_TELE_NO")).clear();
	
	Assert.assertEquals(TeleNum, CNumber);
	
}

//********************* Method to search by account type on credit control tab *******************
public void SearchByAccountType() throws InterruptedException
{
	driver.findElement(By.id("P7_TELE_NO")).clear();
	
	Select dropdown= new Select(driver.findElement(By.id("P7_ACC_TYPE")));
	
	dropdown.selectByVisibleText(AcType);
	
	click_search();
	
	Thread.sleep(2500);
	
	String Atype =driver.findElement(By.xpath("//div[@id='R2806414137352331']//tr[2]//td[8]")).getText();
	
	Select dropdown1= new Select(driver.findElement(By.id("P7_ACC_TYPE")));
	
	dropdown1.selectByVisibleText("-- Select --");
	
	Assert.assertEquals(Atype, AcType);

}
	
//********************** Method to search by tax rate type on credit control tab *****************
public void SearchByTaxRate() throws InterruptedException
{
	Select dropdown= new Select(driver.findElement(By.id("P7_TAX_RATE")));

	dropdown.selectByVisibleText(TaxRate);
	
	click_search();
	
	Thread.sleep(2500);
	
	String Atype =driver.findElement(By.xpath("//div[@id='R2806414137352331']//tr[2]//td[12]")).getText();
	
	Select dropdown1= new Select(driver.findElement(By.id("P7_TAX_RATE")));
	
	dropdown1.selectByVisibleText("-- Select --");
	
	Assert.assertEquals(Atype, TaxRate);
	
	}
	
//********************** Method to search by statement type on credit control tab *****************
public void SearchByStatementType() throws InterruptedException
{
	Select dropdown= new Select(driver.findElement(By.id("P7_STATEMENT_TYPE")));
	
	dropdown.selectByVisibleText("Single");
	
	click_search();
	
	Thread.sleep(2500);
	
	String Atype =driver.findElement(By.xpath("//div[@id='R2806414137352331']//tr[2]//td[9]")).getText();
	
	String Expected= "Single";
	
	Select dropdown1= new Select(driver.findElement(By.id("P7_STATEMENT_TYPE")));
	
	dropdown1.selectByVisibleText("-- Select --");
	
	Assert.assertEquals(Atype, Expected);
	
	}
}
