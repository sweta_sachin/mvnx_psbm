package Billing_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Export_simData_Tab_tests extends Common_Methods.CommonMethods{
	

	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		Thread.sleep(1000);
		click_billingTab();
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
		driver.quit();
	}
	
	@Test
	public void a_View_ExportSimData_Tab()
	{
	  view_ExportSimDataTab();
	}
  
	@Test
	public void b_Click_ExportSimData_Tab() throws InterruptedException
	{
		Thread.sleep(1000);
		click_ExportSimDataTab();
	}
  
	@Test 
	public void c_SearchData_byOrder() throws InterruptedException
	{
		Thread.sleep(400);
		SearchData();
	}
  
 /* @Test
  public void d_ExportData() throws InterruptedException
  {
	  Thread.sleep(400);
  }
  */
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//**************************** Method to view Export Sim Data tab *****************************
public void view_ExportSimDataTab()
{
	WebElement ExportSimData=  driver.findElement(By.linkText("Export Sim Data"));
	
	Boolean ExportSimData_tab= ExportSimData.isDisplayed();
	
	Assert.assertTrue(ExportSimData_tab);
}

//**************************** Method to click Export Sim Data tab *****************************
public void click_ExportSimDataTab() throws InterruptedException
{
	WebElement ExportSimData=  driver.findElement(By.linkText("Export Sim Data"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(ExportSimData).click().build().perform();
	
	Thread.sleep(400);

	//Verify
	
	Boolean verify= driver.findElement(By.id("P34_ORDER_NA_UID")).isDisplayed();
	
	Assert.assertTrue(verify);
	
}

//************************************* Method to search data by selecting order *********************
public void SearchData() throws InterruptedException
{
	//Select order

	Select order= new Select(driver.findElement(By.id("P34_ORDER_NA_UID")));
	
	order.selectByIndex(1);
	
	Thread.sleep(4000);
		
	//Verify
	
	Boolean verify= driver.findElement(By.id("P34_CATEGORY")).isDisplayed();
	
	Assert.assertTrue(verify);

}
//************************************** Method to export the searched data **********************************
public void Export_data() throws InterruptedException
{
	// Click export
	
	WebElement Export=  driver.findElement(By.id("B3822516214604412"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Export).click().build().perform();
	
	Thread.sleep(4000);
	
	// Add more steps after the FE error resolved 

   }
}
