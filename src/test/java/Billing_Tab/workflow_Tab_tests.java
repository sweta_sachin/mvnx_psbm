package Billing_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class workflow_Tab_tests extends  Common_Methods.CommonMethods{

	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		Thread.sleep(1000);
		click_billingTab();
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
		driver.quit();
	}
	
  @Test
  public void a_View_WorkFlow_Tab()
  {
	  view_WorkFlowTab();
  }
  
  @Test
  public void b_ClickWorkFlow_Tab() throws InterruptedException
  {
	 Thread.sleep(1000);
	  click_WorkFlowTab();
  }
  
   @Test
 	public void c_Filter_report() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		View_filtered_report();
 	}
 	
 	@Test
 	public void d_Save_Report() throws InterruptedException
 	{
 		Thread.sleep(1500); 
 		Save_report();
 	}
 	
 	@Test
 	public void e_Search_by_privateReport() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		search_by_private_report();
 	}
 	
 	@Test
 	public void f_search_By_Primary_report() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		search_by_PrimaryReports();
 	}
 	
 	@Test
 	public void g_flashBackTest() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		flashback_filter();
 	}
 	
 	@Test
 	public void h_reset_test() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		reset();
 	}
 	
 	@Test
 	public void i_DownloadCSV() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","billrun_workflows.csv"));
 	}
 	
 	@Test
	public void j_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","billrun_workflows.htm"));
	}
	
	@Test
	public void k_emailReport() throws InterruptedException
	{
		Thread.sleep(300);
		email_report();
	}

	@Test
	public void l_Create_Workflow() throws InterruptedException
	{
		Thread.sleep(300);
		Create_WorkFlows();
	}
	
	@Test
	public void m_View_AvailableWorkflows() throws InterruptedException
	{
		Thread.sleep(300);
		View_Available_Workflow();
	}
	 
	@Test
	public void n_View_StepsOfAvailable_Workflow() throws InterruptedException
	{
		Thread.sleep(300);
		View_Steps(); 
	}
	
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////	
//*************************************** Method to view WorkFlow tab ***********************************
public void view_WorkFlowTab()
{
	WebElement WorkFlow=  driver.findElement(By.linkText("Workflows"));

	Boolean WorkFlow_tab= WorkFlow.isDisplayed();
	
	Assert.assertTrue(WorkFlow_tab);
	
}

//*********************************** Method to click WorkFlow tab ****************************************
public void click_WorkFlowTab()
{
	WebElement WorkFlow=  driver.findElement(By.linkText("Workflows"));

	Actions act= new Actions(driver);
	
	act.moveToElement(WorkFlow).click().build().perform();
	
}
	
//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	expvalue= "Open";
	col= "Status";

	view_filtered_report();
		
	//Verify report
	
	Thread.sleep(400);
	
	String verifyreport= driver.findElement(By.xpath("//tr[@class='even']//td[contains(text(),'Open')]")).getText();
	
	Thread.sleep(300);
	
	Assert.assertEquals(verifyreport, expvalue);
				
	}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{   
	report= "1. Test report"; 

	save_report();
				 	
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
		 	 
	}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	

	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
	
}

//**************************************** Method to search primary report ****************************
public void search_by_PrimaryReports() throws InterruptedException
{
	try {
		
		select_primary_report();
			
		Thread.sleep(300);
			
		WebElement primaryreport= driver.findElement(By.id("124667525423104908"));
			
		Boolean report= primaryreport.isDisplayed();
			
		Assert.assertTrue(report);
			
	} catch(Exception e){
	
		select_primary_report();
		
		Thread.sleep(300);
		
		WebElement primaryreport= driver.findElement(By.id("124667525423104908"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
		}
	}

//************************************ Method to test reset button on CIB ******************************
public void reset() throws InterruptedException
 {
	Thread.sleep(200);

	Reset();
		 
	// Verify report
		
	WebElement primaryreport= driver.findElement(By.id("124667525423104908"));
		
	Boolean report= primaryreport.isDisplayed();
		
	Assert.assertTrue(report);
		
	}
	
//******************************** Method to create work flows *************************** 
public void Create_WorkFlows() throws InterruptedException
  {
	 //Select bill run number
	
	Select billRunNumer= new Select (driver.findElement(By.id("P29_BBR_UID")));
	
	billRunNumer.selectByIndex(2);
	
	Thread.sleep(200);
		
	//Select workflow
	
	Select workflow= new Select (driver.findElement(By.id("P29_WF_UID")));
	
	workflow.selectByVisibleText("Create Bill Run Workflow");
	
	Thread.sleep(200);
		
	//Enter description
	
	WebElement description= driver.findElement(By.id("P29_WFI_DESCRIPTION"));
	
	description.clear();
	
	description.sendKeys(Name);
		
	//Click Create workflow
		
	WebElement create= driver.findElement(By.id("P29_CREATE_WORKFLOW"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(create).click().build().perform();
		
}

//*********************************** Method to view available work flows ************************************
public void View_Available_Workflow()
{
	WebElement report= driver.findElement(By.id("124667525423104908"));
	
	Boolean Report= report.isDisplayed();
	
	Assert.assertTrue(Report);
}

//*********************************** Method to view steps of available work flows *****************************
public void View_Steps() throws InterruptedException
{
	//Click steps
	
	WebElement steps= driver.findElement(By.xpath("//tr[@class='even']//a[contains(text(),'Steps')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(steps).click().build().perform();
	
	Thread.sleep(400);
	   
	 //Verify
	
	WebElement report= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[1]"));
	
	Boolean Report= report.isDisplayed();
	
	Assert.assertTrue(Report);

  }
}
